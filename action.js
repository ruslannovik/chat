
/*
http://asdf/api/
https://

ws://asdf
wss://sadf
*/


const ws = new WebSocket('ws://ws-chat-itclass.herokuapp.com');

let userName;
const modalWrapper = document.querySelector('.wrapper');
const confirmBtn = document.querySelector('.confirm');
const userNameInput = document.querySelector('.user-name');
const messageInput = document.querySelector('.message-text');
const sendBtn = document.querySelector('.send');
const charContainer = document.querySelector('.chat');


userNameInput.focus();

confirmBtn.addEventListener('click', setUserName);
userNameInput.addEventListener('keypress', (event) => enterKeyHandler(event, setUserName));
sendBtn.addEventListener('click', sendMessage);
messageInput.addEventListener('keypress', (event) => enterKeyHandler(event, sendMessage))

function setUserName() {
    const inputValue = userNameInput.value;
    if (inputValue && inputValue.length >= 2) {
        userName = inputValue;
        modalWrapper.style.display = 'none';
        messageInput.focus();
    } else {
        userNameInput.style.border = '1px solid red';
    }
}

function enterKeyHandler({code}, callback) {
    if (code === 'Enter') {
        callback();
    }
}

function sendMessage() {
    const message = messageInput.value;
    if (message && message.length > 0 && userName && userName.length > 2) {
        const messageObj = {
            name: userName,
            text: message,
            time: new Date().getTime()
        };

        ws.send(JSON.stringify(messageObj));
        messageInput.value = '';
        messageInput.focus();
    }
}

function generateMessageTag(message) {
    const itemDiv = document.createElement('div');
    itemDiv.classList.add('item');
    if (message.name !== userName) {
        itemDiv.classList.add('item_alien');
    }

    const messageDiv = document.createElement('div');
    messageDiv.classList.add('message');

    const vendorDiv = document.createElement('div');
    vendorDiv.classList.add('vendor');
    vendorDiv.innerText = message.name;

    const textDiv = document.createElement('div');
    textDiv.classList.add('text');
    textDiv.innerText = message.text;
    
    const timeDiv = document.createElement('div');
    timeDiv.classList.add('time');
    timeDiv.innerText = formattedTime(message.time);

    messageDiv.append(vendorDiv, textDiv, timeDiv);
    itemDiv.appendChild(messageDiv);
    return itemDiv;
}

function formattedTime(time) {
    const date = new Date();
    date.setTime(time);
    const hours = date.getHours();
    const minutes = date.getMinutes();
    return `${hours < 10 ? '0' + hours : hours}:${minutes < 10 ? '0' + minutes : minutes}`;
}

ws.onmessage = (message) => {
    const messageObj = JSON.parse(message.data);
    const messageTag = generateMessageTag(messageObj);
    charContainer.appendChild(messageTag);
    messageTag.scrollIntoView({behavior: "smooth"});
}

